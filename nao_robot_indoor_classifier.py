# encoding:utf-8
import requests
import json
from nao_robot import Robot
import time

ROBOT = None

def run(nao_ip, nao_port):
    list_scenes = ["soccer_court", "exit", "desks", "office"]
    headers = {
    'content-type': "application/json",
    'authorization': "zm9DxXxul8yFcSWPgsalXKJvyBhUP8aArXFZmge3nscxkAS0KP"
    }
    ROBOT = Robot(nao_ip, nao_port)
    ROBOT.subscribe_to_camera()
    choosed_op = input("1)Cancha\n2)Salida\n3)Escritorios\n4)Oficina\n")
    while 0 < choosed_op < 5 :
        print("Clase, Predicción, Tiempo, Correcta")
        for i in range(10):
            image_from_robot = ROBOT.get_image_from_robot()
            if image_from_robot:
                with open(list_scenes[choosed_op - 1] + \
                    str(i) + ".txt", "w") as file:
                    file.write(image_from_robot)
                data = {"imageContent" : image_from_robot,\
                "features" : [{"type" : "CLASSIFY_INDOOR_SCENES"}]}
                t_start = time.time()
                r = requests.post(url="http://132.248.180.17/vision", \
                    headers=headers, data=json.dumps(data))
                r_json = r.json()
                t_all = time.time() - t_start
                area = r_json.get("features").get("indoorScenesClassify").\
                get("indoor_scene")
                
                print(list_scenes[choosed_op - 1] + "," +\
                    area + "," + str(t_all) + "," + \
                    str(area == list_scenes[choosed_op - 1]))
                ROBOT.say_speech("Cambio")
            time.sleep(3)       
        choosed_op = input("1)Cancha\n2)Salida\n3)Escritorios\n4)Oficina")
    ROBOT.unsubscribe_to_camera()
            


