
# coding: utf-8
"""
.. module:: cnn_indoor_classifier_model
   :platform: Unix, Windows
   :synopsis: Módulo de la CNN.

.. moduleauthor:: Ivan Feliciano <ivan.felavel@gmail.com>

"""
import tensorflow as tf
import cv2
import glob
import time
import numpy as np


def hot_encoding_from_category(val, number_of_categories=4):
    """
    Función para crear un vector one-hot dado un número de
    categorías y el índice de la clase correcta.
    
    :param val: el número de la clase (índice de la categoría).
    :type val: int.
    :param number_of_categories: el número de categorías 4 por defecto.
    :type number_of_categories: int.
    :returns: un arreglo de ``numpy`` que es el vector one-hot.

    """
    ans = np.zeros((number_of_categories))
    ans[val] = 1
    return ans


def list_files_in_directory(directory_name):
    """
    Crea una lista con las rutas de todos los archivos dentro de un
    directorio.

    :param directory_name: la ruta del directorio relativa al espacio de trabajo.
    :type directory_name: str.
    :returns: list -- una lista de con las rutas de los archivos en el directorio dado.

    """
    return glob.glob(directory_name + "/*")


def get_labels_from_path(file_path):
    """
    Crea un arreglo de numpy con las categoría de
    cada imagen según la carpeta en la que se encuentra.
    Utiliza el one-hot encoding para representar
    las categorías, esto es:
    
    + ``[1, 0, 0, 0]``, la imagen es de la categoría *desks*.
    + ``[0, 1, 0, 0]``, la imagen es de la categoría *exit*.
    + ``[0, 0, 1, 0]``, la imagen es de la categoría *office*.
    + ``[0, 0, 0, 1]``, la imagen es de la categoría *soccer_court*.

    :param file_path: La ruta del archivo.
    :type file_path: str.
    
    """

    labels_he = []
    for x in file_path:
        val = 0
        if x.strip().split('/')[2] == 'desks':
            val = 0
        if x.strip().split('/')[2] == 'exit':
            val = 1
        if x.strip().split('/')[2] == 'office':
            val = 2
        if x.strip().split('/')[2] == 'soccer_court':
            val = 3
        labels_he.append(hot_encoding_from_category(val))
    return np.array(labels_he)


def init_weights(shape):
    """
    Retorna una variable de TensorFlow con las dimensiones 
    que se pasen como argumento. 

    La variable representa los pesos en una red neuronal,
    y estos se inicializan con una distribución normal, 
    con media cero y desviación estándar igual a 0.1.

    :param shape: Las dimensiones de la variable.
    :type shape: list.
    :returns: Una variable de TensorFlow. 

    """
    return tf.Variable(tf.truncated_normal(shape, stddev=0.1))


def init_bias(shape):
    """
    Retorna una variable de TensorFlow con las dimensiones 
    que se pasen como argumento. 

    La variable representa los sesgos en una red neuronal,
    y estos se inicializan con un valor constante igual a 0.1.

    :param shape: Las dimensiones de la variable.
    :type shape: list.
    :returns: Una variable de TensorFlow. 
    
    """
    return tf.Variable(tf.constant(0.1, shape=shape))


def conv2d(x, W):
    """
    Realiza la convolución de un tensor que representa la entrada
    y otro con los kernels. La zancada es igual a 1 y el borde de
    ceros es **same**. Utiliza la función ``conv2d`` de la API de
    TensorFlow.

    :param x: La entrada de la convolución.
    :type x: tensor.
    :param W: El tensor con los kernels.
    :type W: tensor.
    :returns: Un tensor con el resultado de la operación de convolución.

    """
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_2by2(x):
    """
    Realiza el agrupamiento máximo con vecidades de 2 por 2.
    Usa el método ``max_pool`` de TF.

    :param x: El tensor al que se le aplica el agrupamiento máximo.
    :type x: tensor.
    :returns: El resultado de la operación de agrupamiento.

    """
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1], padding='SAME')


def convolutional_layer(input_x, shape):
    """
    Realiza las operaciones que se hacen en una capa
    de convolución. La convolución el agrupamiento
    y la aplicación de la función de activación ReLU.

    :param input_x: La entrada de la convolución.
    :type input_x: tensor.
    :param shape: Una lista con el tamaño de los kernels, el número de mapas de características de entrada y el número de mapas de caracterísitcas de salida.
    :returns: Un tensor con los resultados de las operaciones de la capa de convolución.

    """
    W = init_weights(shape)
    b = init_bias([shape[3]])
    return tf.nn.relu(conv2d(input_x, W) + b)


def normal_full_layer(input_layer, size):
    """
    Realiza las operaciones de una capa completamente 
    conectada en una red neuronal, sin aplicar
    la función de activación, sólo la multiplicación 
    de matrices y la suma del sesgo.
    
    :param input_layer: La entrada de la capa.
    :type input_layer: tensor.
    :param size: el número de unidades de la capa.
    :type size: int.
    :returns: El resultado de la multiplicación de una matriz de pesos y la entrada de la capa, más un vector de sesgos.

    """
    input_size = int(input_layer.get_shape()[1])
    W = init_weights([input_size, size])
    b = init_bias([size])
    return tf.matmul(input_layer, W) + b


class CNNClassifierLAR(object):
    """
    La clase que representa una arquitectura de red convolucional con a lo más
    dos capas de convolución y dos capas completamente conectadas.

    La red está creada para específicamente aceptar entradas de dimensiones
    de 32 pixeles por 32 pixeles por 3 canales. El constructor recibe tres parámetros
    una lista con las dimensiones de los kernels, entradas de la convolución y
    número de mapas de características, un entero con el número de unidades en la
    capa oculta y un valor flotante que es la tasa de aprendizaje.
    En el siguiente ejemplo se crea una red con dos capas de convolución,
    una capa oculta con 2048 unidades y una tasa de aprendizaje de 0.01. La ejecución de la gráfica
    para el aprendizaje por lotes recibe como parámetros 200 y 3000,
    el tamaño del lote y el número de épocas.
    
    >>> from cnn_indoor_classifier_model import CNNClassifierLAR
    >>> clasificador = CNNClassifierLAR([[5, 5, 3, 16], [3, 3, 16, 32]], 2048, 0.01)
    >>> clasificador.create_graph_2_convo_layers()
    >>> clasificador.run_graph(200, 3000)
    i; time; training loss; test loss; accuracy
    0;0.7688136100769043;1.5798137187957764;1.4590167999267578;0.13778409361839294
    ...
    3000;68.31876397132874;0.0036288381088525057;0.11022621393203735;0.9692234992980957
    Time 68.3188087940216

    """

    BATCH_IDX = 0

    def __init__ (self, shape_convo_layers, units_fc, learning_rate):
        self.ls_shape_convo_layers = shape_convo_layers
        self.units_fc = units_fc
        self.input_x_ph = tf.placeholder(tf.float32, shape=[None, 32, 32, 3],\
            name="input_x")
        self.y_true_ph = tf.placeholder(tf.float32, shape=[None, 4])
        self.optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)

    def next_batch(self, batch_size):
        """
        Método para generar un lote del conjunto de entrenamiento
        junto con sus etiquetas.

        :param batch_size: El tamaño del lote.
        :type batch_size: int.
        :return: El lote del conjunto de entrenamiento y sus etiquetas.
        :rtype: Una dupla de listas con las imágenes de entrenamiento y sus etiquetas.

        """
        x = self.training_images[self.BATCH_IDX:self.BATCH_IDX + batch_size]
        y = self.labels_training_set[self.BATCH_IDX: self.BATCH_IDX + batch_size]
        self.BATCH_IDX = (self.BATCH_IDX + batch_size) % len(self.training_images)
        return x, y

    def get_training_and_test_images(self, dataset_training_path='dataset/training_set/*',\
                                     dataset_test_path='dataset/test_set/*'):
        """
        Llena las listas con las imágenes de entrenamiento y 
        de prueba y sus repectivas etiquetas.
        
        :param dataset_training_path: La ruta relativa al directorio de trabajo donde se encuentra el conjunto de entrenamiento.
        :type dataset_training_path: numpy.array.
        :param dataset_test_path: La ruta relativa al directorio de trabajo donde se encuentran las imágenes del conjunto de prueba.
        :type dataset_test_path: numpy.array.

        """
        training_paths = np.array(
            list_files_in_directory(dataset_training_path))
        np.random.shuffle(training_paths)
        self.labels_training_set = get_labels_from_path(training_paths)
        testing_paths = np.array(list_files_in_directory(dataset_test_path))
        np.random.shuffle(testing_paths)
        self.labels_test_set = get_labels_from_path(testing_paths)
        self.training_images = np.array([cv2.cvtColor(cv2.resize(cv2.imread(
            file_name), (32, 32)), cv2.COLOR_BGR2RGB) / 255 for file_name in training_paths])
        self.testing_images = np.array([cv2.cvtColor(cv2.resize(cv2.imread(
            file_name), (32, 32)), cv2.COLOR_BGR2RGB) / 255 for file_name in testing_paths])
    
    def create_graph_2_convo_layers(self, two_fc=True):
        """
        Método para construir la gráfica de cómputo que representa a un modelo
        con dos capas de convolución y a través de del argumento agregar una capa
        oculta o no.

        :param two_fc: Una bandera que indica si se agrega una capa oculta.
        :type two_fc: boolean. 
        """

        convo_1 = convolutional_layer(self.input_x_ph, shape=self.ls_shape_convo_layers[0])
        convo_1_pooling = max_pool_2by2(convo_1)
        convo_2 = convolutional_layer(convo_1_pooling, shape=self.ls_shape_convo_layers[1])
        convo_2_pooling = max_pool_2by2(convo_2)
        last_maps_of_features = self.ls_shape_convo_layers[1][3]
        convo_2_flat = tf.reshape(convo_2_pooling, [-1, 8 * 8 * last_maps_of_features])
        if two_fc:
            full_layer_one = tf.nn.relu(normal_full_layer(convo_2_flat, self.units_fc))
        else:
            full_layer_one = convo_2_flat
        self.y_pred = tf.identity(normal_full_layer(full_layer_one, 4), name="y_pred")
        self.cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=self.y_true_ph, logits=self.y_pred))
        self.train = self.optimizer.minimize(loss=self.cross_entropy, global_step=tf.train.get_global_step())

    def create_graph_1_convo_layer(self, two_fc=True):
        """
        Método para contruir la gráfica de cómputo de una arquitectura
        con una capa de convolución y ya sea una o dos capas completamente
        conectadas.

        :param two_fc: Bandera para indicar si hay dos capas completamente conectadas.
        :type two_fc: boolean.

        """
        convo_1 = convolutional_layer(
            self.input_x_ph, shape=self.ls_shape_convo_layers)
        convo_1_pooling = max_pool_2by2(convo_1)
        last_maps_of_features = self.ls_shape_convo_layers[3]
        convo_2_flat = tf.reshape(
            convo_1_pooling, [-1, 16 * 16 * last_maps_of_features])
        if two_fc:
            full_layer_one = tf.nn.relu(
                normal_full_layer(convo_2_flat, self.units_fc))
        else:
            full_layer_one = convo_2_flat
        self.y_pred = tf.identity(normal_full_layer(
            full_layer_one, 4), name="y_pred")
        self.cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
            labels=self.y_true_ph, logits=self.y_pred))
        self.train = self.optimizer.minimize(
            loss=self.cross_entropy, global_step=tf.train.get_global_step())

        
    def run_graph(self, batch_size, epochs):
        """
        Método para ejecutar la gráfica de cómputo. Realiza el entrenamiento
        por lotes y evalua la red para conocer la precisión del modelo
        cada 100 épocas.

        :param batch_size: El tamaño del lote.
        :type batch_size: int.
        :param epochs: El número de épocas.
        :type epochs: int.

        """

        self.get_training_and_test_images()
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver()
            #writer = tf.summary.FileWriter("./logs_tensorboard", sess.graph)
            t_start = time.time()
            print("i; time; training loss; test loss; accuracy")
            for i in range(epochs + 1):
                batch_x, batch_y = self.next_batch(batch_size)
                _, loss = sess.run([self.train, self.cross_entropy], feed_dict={self.input_x_ph: batch_x, self.y_true_ph: batch_y})
                if i % 100 == 0:
                    matches = tf.equal(tf.argmax(self.y_pred, 1), tf.argmax(self.y_true_ph, 1))
                    acc = tf.reduce_mean(tf.cast(matches, tf.float32))
                    te_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=self.y_true_ph, logits=self.y_pred))
                    accuracy, test_loss =sess.run([acc, te_loss], feed_dict={
                        self.input_x_ph: self.testing_images, self.y_true_ph: self.labels_test_set})
                    print("{};{};{};{};{}".format(i, time.time() - t_start ,loss, test_loss, accuracy))
                    saver.save(sess, 'best_model/image_classifier.ckpt')
            print("Tiempo {}".format(time.time() - t_start))


best_model = CNNClassifierLAR([[5, 5, 3, 16], [3, 3, 16, 32]], 1024, 0.01)
best_model.create_graph_2_convo_layers()
best_model.run_graph(200, 2000)

# epochs_ls = [500, 1000, 2000]
# learning_rate_ls = [0.0001, 0.001, 0.01]
# batch_sizes = [100, 200, 300]
# conv2_shapes = [[[[7, 7, 3, 8], [5, 5, 8, 16]], [[5, 5, 3, 8], [3, 3, 8, 16]]],\
#                 [[[7, 7, 3, 16], [5, 5, 16, 32]], [[5, 5, 3, 16], [3, 3, 16, 32]]]]
# convo1_shapes = [[5, 5, 3, 8], [5, 5, 3, 16], [5, 5, 3, 32]]
# fc_units = [1024, 512, 128]

# net_idx = 1 
#2 convo layers 2 FC
# for epoch in epochs_ls:
#     for learning_rate in learning_rate_ls:
#         for batch_size in batch_sizes:
#             for conv_shape in conv2_shapes:
#                 for shape in conv_shape:
#                     for units in fc_units:
#                         print("Network {}\n epoch: {}\n learning rate: {} \n batch size: {}\n conv_shape: {}\n units: {}".format(\
#                             net_idx, epoch, learning_rate, batch_size, shape, units))
#                         net_idx += 1
#                         current_net = CNNClassifierLAR(shape, units, learning_rate)
#                         current_net.create_graph_2_convo_layers()
#                         current_net.run_graph(batch_size, epoch)

#1 convo layers 2 FC
# print("1 Convolutional layer 2 FC")
# for epoch in epochs_ls:
#     for learning_rate in learning_rate_ls:
#         for batch_size in batch_sizes:
#             for conv_shape in convo1_shapes:
#                 for units in fc_units:
#                     print("Network {}\n epoch: {}\n learning rate: {}\n batch size: {}\n conv_shape: {}\n units: {}".format(
#                         net_idx, epoch, learning_rate, batch_size, conv_shape, units))
#                     net_idx += 1
#                     current_net = CNNClassifierLAR(
#                         conv_shape, units, learning_rate)
#                     current_net.create_graph_1_convo_layer()
#                     current_net.run_graph(batch_size, epoch)

# #2 convo layers 1 FC
# print("2 convo layers 1 FC")
# for epoch in epochs_ls:
#     for learning_rate in learning_rate_ls:
#         for batch_size in batch_sizes:
#             for conv_shape in conv2_shapes:
#                 for shape in conv_shape:
#                     print("Network {}\n epoch: {}\n learning rate: {}\n batch size: {}\n conv_shape: {}".format(
#                         net_idx, epoch, learning_rate, batch_size, shape))
#                     net_idx += 1
#                     current_net = CNNClassifierLAR(
#                         shape, 0, learning_rate)
#                     current_net.create_graph_2_convo_layers(False)
#                     current_net.run_graph(batch_size, epoch)

# 1 convo layers 1 FC
# print("1 Convolutional layer 1 FC")
# for epoch in epochs_ls:
#     for learning_rate in learning_rate_ls:
#         for batch_size in batch_sizes:
#             for conv_shape in convo1_shapes:
#                 print("Network {}\n epoch: {}\n learning rate: {}\n batch size: {}\n conv_shape: {}".format(
#                     net_idx, epoch, learning_rate, batch_size, conv_shape))
#                 net_idx += 1
#                 current_net = CNNClassifierLAR(
#                     conv_shape, 0, learning_rate)
#                 current_net.create_graph_1_convo_layer(False)
#                 current_net.run_graph(batch_size, epoch)

