# Modelo para clasificación de imágenes en distintos escenarios.


En este proyecto se creó una red neuronal convolucional, que
recibiera como entrada un arreglo con los pixeles de una imagen tomada por el
robot, y la salida fuera la categoría a la que pertenece esa imagen. Las clases en las
que se desea clasificar las imágenes son lugares alrededor del Laboratorio de Algo-
ritmos para la Robótica, que se ubica en el cubículo
15 del Centro de Desarrollo
Tecnológico de la FES Acatlán. Se eligieron las siguientes cuatro zonas:

* El cubículo.
* La salida de emergencia.
* La cancha de entrenamiento de fútbol para el robot NAO.
* Zona de trabajo del Laboratorio

# Descripción

Para utilizar los módulos de entrenamiento y libretas de Jupyter
se ejecuta un ambiente virtual en Ubuntu.

```bash

virtualenv -p python3 my_venv
source my_venv/bin/activate
pip install -r requirements.txt

```
En el script `cnn_indoor_classifier_model.py` se realiza la creación, entrenamiento y ejecución de modelos.

Para conectarse con la API REST de CloudNAO desde el robot, no es necesario un ambiente virtual, simplemente se ejecuta la siguiente aplicación:

```bash

python app.py --ip IP_DEL_ROBOT --port PUERTO_DEL_ROBOT

```